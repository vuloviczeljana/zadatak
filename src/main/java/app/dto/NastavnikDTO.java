package app.dto;

public class NastavnikDTO {
	
	private Long id;
	private String ime;
	private String prezime;
	private String biografija;
	private int jmbg;
	
	public NastavnikDTO() {
		super();
	}
	
	public NastavnikDTO(Long id, String ime, String prezime, String biografija, int jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getBiografija() {
		return biografija;
	}
	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}
	public int getJmbg() {
		return jmbg;
	}
	public void setJmbg(int jmbg) {
		this.jmbg = jmbg;
	}
	
	

}
