package app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import app.model.Nastavnik;
import app.repository.NastavnikRepository;
import app.service.NastavnikService;

@Controller
@RequestMapping(path="api/nastavnici")
public class NastavnikController {
	@Autowired
	NastavnikService nastavnikService;

	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Nastavnik>> dobaviNastavnike() {
		return new ResponseEntity<Iterable<Nastavnik>>(nastavnikService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Nastavnik> dobaviNastavnika(@PathVariable("id") Long id) {
		Optional<Nastavnik> n = nastavnikService.findOne(id);
		if (n != null)
			return new ResponseEntity<Nastavnik>(HttpStatus.OK);
		else
			return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);
	}


	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Nastavnik> save(@RequestBody() Nastavnik n) {
		nastavnikService.save(n);
		return new ResponseEntity<Nastavnik>(n, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Nastavnik> remove(@PathVariable("id") Long id) {
		Optional<Nastavnik> n = nastavnikService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);
		}
		nastavnikService.remove(id);
		return new ResponseEntity<Nastavnik>(HttpStatus.OK);
	} 
	
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Nastavnik>> edit(@RequestBody() Nastavnik n) {
		Optional<Nastavnik> nastavnik = nastavnikService.findOne(n.getId());
		if (nastavnik == null) {
			return new ResponseEntity<Optional<Nastavnik>>(HttpStatus.NOT_FOUND);
		}
		nastavnikService.save(n);
		return new ResponseEntity<Optional<Nastavnik>>(HttpStatus.OK);
	}
	
} 