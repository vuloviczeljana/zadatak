package app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.NaucnaOblast;
import app.model.NaucnaOblast;
import app.service.NaucnaOblastService;
import app.service.NaucnaOblastService;


@Controller
@RequestMapping(path="api/naucna-oblast")
public class NaucnaOblastController {

	@Autowired
	NaucnaOblastService naucnaOblastService; 

	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<NaucnaOblast>> dobaviNaucnuOblast() {
		return new ResponseEntity<Iterable<NaucnaOblast>>(naucnaOblastService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<NaucnaOblast> dobaviNaucnuOblast(@PathVariable("id") Long id) {
		Optional<NaucnaOblast> n = naucnaOblastService.findOne(id);
		if (n != null)
			return new ResponseEntity<NaucnaOblast>(HttpStatus.OK);
		else
			return new ResponseEntity<NaucnaOblast>(HttpStatus.NOT_FOUND);
	}


	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<NaucnaOblast> save(@RequestBody() NaucnaOblast n) {
		naucnaOblastService.save(n);
		return new ResponseEntity<NaucnaOblast>(n, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<NaucnaOblast> remove(@PathVariable("id") Long id) {
		Optional<NaucnaOblast> n = naucnaOblastService.findOne(id);
		if (n == null) {
			return new ResponseEntity<NaucnaOblast>(HttpStatus.NOT_FOUND);
		}
		naucnaOblastService.remove(id);
		return new ResponseEntity<NaucnaOblast>(HttpStatus.OK);
	}
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<NaucnaOblast>> edit(@RequestBody() NaucnaOblast n) {
		Optional<NaucnaOblast> naucnaOblast = naucnaOblastService.findOne(n.getId());
		if (naucnaOblast == null) {
			return new ResponseEntity<Optional<NaucnaOblast>>(HttpStatus.NOT_FOUND);
		}
		naucnaOblastService.save(n);
		return new ResponseEntity<Optional<NaucnaOblast>>(HttpStatus.OK);
	}
} 
