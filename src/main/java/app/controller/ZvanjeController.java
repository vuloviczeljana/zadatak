package app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Nastavnik;
import app.model.Zvanje;
import app.service.ZvanjeService;
import app.service.ZvanjeService;

public class ZvanjeController {
	@Autowired
	ZvanjeService zvanjeService;
	
	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Zvanje>> dobaviZvanje() {
		return new ResponseEntity<Iterable<Zvanje>>(zvanjeService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Zvanje> dobaviZvanje(@PathVariable("id") Long id) {
		Optional<Zvanje> n = zvanjeService.findOne(id);
		if (n != null)
			return new ResponseEntity<Zvanje>(HttpStatus.OK);
		else
			return new ResponseEntity<Zvanje>(HttpStatus.NOT_FOUND);
	}


	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Zvanje> save(@RequestBody() Zvanje n) {
		zvanjeService.save(n);
		return new ResponseEntity<Zvanje>(n, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Zvanje> remove(@PathVariable("id") Long id) {
		Optional<Zvanje> n = zvanjeService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Zvanje>(HttpStatus.NOT_FOUND);
		}
		zvanjeService.remove(id);
		return new ResponseEntity<Zvanje>(HttpStatus.OK);
	}
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Zvanje>> edit(@RequestBody() Zvanje n) {
		Optional<Zvanje> zvanje = zvanjeService.findOne(n.getId());
		if (zvanje == null) {
			return new ResponseEntity<Optional<Zvanje>>(HttpStatus.NOT_FOUND);
		}
		zvanjeService.save(n);
		return new ResponseEntity<Optional<Zvanje>>(HttpStatus.OK);
	}
}
