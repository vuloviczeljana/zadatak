package app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Zvanje;
import app.model.TipZvanja;
import app.model.Zvanje;
import app.service.TipZvanjaService;
import app.service.TipZvanjaService;

public class TipZvanjaController {

	@Autowired
	TipZvanjaService tipZvanjaService;
	
	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<TipZvanja>> dobaviZvanje() {
		return new ResponseEntity<Iterable<TipZvanja>>(tipZvanjaService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<TipZvanja> dobaviZvanje(@PathVariable("id") Long id) {
		Optional<TipZvanja> n = tipZvanjaService.findOne(id);
		if (n != null)
			return new ResponseEntity<TipZvanja>(HttpStatus.OK);
		else
			return new ResponseEntity<TipZvanja>(HttpStatus.NOT_FOUND);
	}


	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<TipZvanja> save(@RequestBody() TipZvanja n) {
		tipZvanjaService.save(n);
		return new ResponseEntity<TipZvanja>(n, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<TipZvanja> remove(@PathVariable("id") Long id) {
		Optional<TipZvanja> n = tipZvanjaService.findOne(id);
		if (n == null) {
			return new ResponseEntity<TipZvanja>(HttpStatus.NOT_FOUND);
		}
		tipZvanjaService.remove(id);
		return new ResponseEntity<TipZvanja>(HttpStatus.OK);
	}
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<TipZvanja>> edit(@RequestBody() TipZvanja n) {
		Optional<TipZvanja> tipZvanja = tipZvanjaService.findOne(n.getId());
		if (tipZvanja == null) {
			return new ResponseEntity<Optional<TipZvanja>>(HttpStatus.NOT_FOUND);
		}
		tipZvanjaService.save(n);
		return new ResponseEntity<Optional<TipZvanja>>(HttpStatus.OK);
	}
	
}
