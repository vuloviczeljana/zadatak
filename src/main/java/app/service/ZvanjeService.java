package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import app.model.Zvanje;
import app.repository.ZvanjeRepository;

public class ZvanjeService {
	@Autowired
	private ZvanjeRepository zvanjeRepository;
	
	public Iterable<Zvanje> findAll(){
		return zvanjeRepository.findAll();
		}
	
	public Iterable<Zvanje> findAll(Pageable pageable){
		return zvanjeRepository.findAll(pageable);
		
	}
	public Optional<Zvanje> findOne(Long id){
		return zvanjeRepository.findById(id);
		}
	
	public void remove(Long id){
		zvanjeRepository.deleteById(id);
	}
	public Zvanje save(Zvanje zvanje){
		return zvanjeRepository.save(zvanje);
		
	}

}
