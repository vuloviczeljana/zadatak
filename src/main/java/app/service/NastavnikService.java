package app.service;


import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Nastavnik;
import app.repository.NastavnikRepository;

@Service
public class NastavnikService {
	@Autowired
	private NastavnikRepository nastavnikRepository;
	
	public Iterable<Nastavnik> findAll(){
		return nastavnikRepository.findAll();
		}
	
	public Iterable<Nastavnik> findAll(Pageable pageable){
		return nastavnikRepository.findAll(pageable);
		
	}
	public Optional<Nastavnik> findOne(Long id){
		return nastavnikRepository.findById(id);
		}
	
	public void remove(Long id){
		nastavnikRepository.deleteById(id);
	}
	public Nastavnik save(Nastavnik nastavnik){
		return nastavnikRepository.save(nastavnik);
		
	}


}
