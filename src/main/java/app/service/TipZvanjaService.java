package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import app.model.TipZvanja;
import app.repository.TipZvanjaRepository;

public class TipZvanjaService {
	@Autowired
	private TipZvanjaRepository tipZvanjaRepository;
	
	public Iterable<TipZvanja> findAll(){
		return tipZvanjaRepository.findAll();
		}
	
	public Iterable<TipZvanja> findAll(Pageable pageable){
		return tipZvanjaRepository.findAll(pageable);
		
	}
	public Optional<TipZvanja> findOne(Long id){
		return tipZvanjaRepository.findById(id);
		}
	
	public void remove(Long id){
		tipZvanjaRepository.deleteById(id);
	}
	public TipZvanja save(TipZvanja tipZvanja){
		return tipZvanjaRepository.save(tipZvanja);
		
	}

}
