package app.service;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.NaucnaOblast;
import app.repository.NaucnaOblastRepository;

@Service
public class NaucnaOblastService {
	@Autowired
	private NaucnaOblastRepository naucnaOblastRepository;
	
	
	public Iterable<NaucnaOblast> findAll(){
		return naucnaOblastRepository.findAll();
		}
	
	public Iterable<NaucnaOblast> findAll(Pageable pageable){
		return naucnaOblastRepository.findAll(pageable);
		
	}
	public Optional<NaucnaOblast> findOne(Long id){
		return naucnaOblastRepository.findById(id);
		}
	
	public void remove(Long id){
		naucnaOblastRepository.deleteById(id);
	}
	public NaucnaOblast save(NaucnaOblast NaucnaOblast){
		return naucnaOblastRepository.save(NaucnaOblast);
		
	}


}
