package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.TipZvanja;

@Repository
public interface TipZvanjaRepository extends PagingAndSortingRepository<TipZvanja, Long>{

}
