package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.NaucnaOblast;

@Repository
public interface NaucnaOblastRepository extends PagingAndSortingRepository<NaucnaOblast, Long>{

}
