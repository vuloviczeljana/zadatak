package app.repository;

import app.model.Nastavnik;

import java.awt.print.Pageable;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NastavnikRepository extends PagingAndSortingRepository<Nastavnik, Long>{


}
